# Kibana utils

Ce projet contient plusieurs scripts (et de la documentation) pour aider à déployer simplement et automatiquement des tableaux de bords sur Kibana.

## Installation

Le fichier `./docker/docker-compose.yml` permet de démarrer une stack avec ElasticSearch et Kibana en local, bien configurée, et avec les bons utilisateurs déjà créés.

On peut par exemple construire la stack avec cette commande:

```bash
cd kibana-utile/docker
docker compose -p elk up -d --build
```

Ensuite, pour accéder à Kibana, on peut aller sur [localhost:5601](http://localhost:5601) et se connecter avec le login `admin` et le mot de passe spécifié sous `ADMIN_PASSWORD` dans le fichier `./docker/.env`.

## Scripts

Pour utiliser les divers scripts _TypeScript_ disponibles, il faut installer une version récente de Node.js en local, puis aller dans le répertoire `scripts` et installer les dépendances :

```bash
cd ./scripts
npm install
```

### Indexer des données

Le script `index-csv` permet d'indexer un fichier CSV donnée dans ElasticSearch, en inférant les types pour avoir des _mappings_ propres à utiliser ensuite dans Kibana pour les représentations de données.

```bash
npm run index-csv -- --data ./data/my-file.csv --index mon-index-es
```

### Importer un dashboard à partir d'un dump `.ndjson`

Kibana permet d'exporter tout un dashboard (et ses dépendances) dans un fichier au format `.ndjson`. Pour cela, à partir d'un Kibana avec un dashboard déjà bien configuré :

1. Aller à l'adresse `http://0.0.0.0:5601/app/management/kibana/objects`
2. Filtrer sur le type `dashboard`
3. Sélectionner le dashboard à exporter
4. Cliquer sur `Export`, puis sur le nouveau bouton `Export` (en s'assurant que `Include related objects` soit bien coché)

À ce stade, vous devez avoir un nouveau fichier `export.ndjson` en local chez vous.

Le script `import-dashboard` de ce projet permet de le réimporter, mais en le branchant sur un autre index arbitraire.

```bash
npm run import-dashboard -- --dashboard ./exports/my-export.ndjson --index mon-index-es
```

L'adresse du nouveau dashboard sera loggué en fin d'exécution du script.

### Créer un _space_

Pour pouvoir avoir des utilisateurs n'ayant que le droit de voir un seul dashboard, la façon la plus simple est de passer par un _space_. ElasticSearch gère les droits des utilisateurs par espace, via un système de rôles assez complexes.

Le script `add-space` permet justement de créer un espace et un rôle dédié qui donnera accès aux utilisateurs de l'espace à tous les tableaux de bord de l'espace, ainsi qu'à l'index, en lecture (c'est nécessaire pour pouvoir lire les dashboards).

```bash
npm run add-space -- --name "Mon espace" --slug mon-espace --index mon-index-es
```

### Créer un utilisateur avec accès en lecture dans un space

Le script `add-user` permet de créer un utilisateur en lui assignant le rôle précédemment créé, qui donne accès en lecture au _space_, au dashboard et à l'index:

```bash
npm run add-user -- --username john-doe --password 12345678 --space mon-espace
```

### Copier un dashboard en changeant d'index

Le script `copy-dashboard` permet de copier un dashboard existant sur un _space_ existant, et en le branchant sur un autre index :

```bash
npm run copy-dashboard -- --space "Mon client" --dashboard xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx --index mon-index-es
```

Je n'ai pas de moyen de "binder" le _space_ à son index pour le moment, il est donc nécessaire de bien respécifier les deux dans la commande.

Aussi, pour connaître l'identifiant du dashboard, il est visible dans son URL, juste après `#/view/` et jusqu'à `?`.

Enfin, l'adresse du nouveau dashboard sera loggué en fin d'exécution du script.

## Questions en suspens

### Avoir une _timeline_ qui affiche toujours toute la période

Ça nécessite le développement d'un plugin ad-hoc. Je ne sais pas encore combien de temps ça prend.

### Avoir une liste de documents avec un rendu custom

Ça nécessite le développement d'un plugin ad-hoc. Je ne sais pas encore combien de temps ça prend.

### Personnaliser l'affichage

C'est assez simple d'injecter de la CSS custom dans Kibana, via un plugin ad-hoc.
