import { deburr } from 'lodash';

export function slugify(str: string): string {
  return deburr(str)
    .toLowerCase()
    .trim()
    .replace(/[^a-zA-Z0-9]+/g, '-');
}
