/**
 * Utils types:
 */
export type JSONString = string;
export type VersionString = string;
export type UUIDString = string;
export type DateString = string;

export interface Reference {
  id: UUIDString;
  name: string;
  type: SavedObjectType;
}

/**
 * Saved object types:
 */
export interface BaseSavedObject {
  type: string;
  attributes: {};
  coreMigrationVersion: VersionString;
  id: UUIDString;
  migrationVersion: {
    dashboard: VersionString;
  };
  references?: Reference[];
  updated_at?: DateString;
  version?: string;
}

export interface DashboardSavedObject extends BaseSavedObject {
  type: 'dashboard';
  attributes: {
    description: string;
    hits: number;
    kibanaSavedObjectMeta: {
      searchSourceJSON: JSONString;
    };
    optionsJSON: JSONString;
    panelsJSON: JSONString;
    timeRestore: boolean;
    title: string;
    version: number;
  };
}

export interface IndexPatternSavedObject extends BaseSavedObject {
  type: 'index-pattern';
  attributes: {
    fieldAttrs: JSONString;
    fieldFormatMap: JSONString;
    fields: JSONString;
    runtimeFieldMap: JSONString;
    timeFieldName?: string | null;
    title: string;
    typeMeta: JSONString;
  };
}

export interface SearchSavedObject extends BaseSavedObject {
  type: 'search';
  attributes: {
    columns: string[];
    description: string;
    grid: {
      columns: {
        _score: {
          width: number;
        };
      };
    };
    hideChart: true;
    kibanaSavedObjectMeta: {
      searchSourceJSON: JSONString;
    };
    rowHeight: number;
    sort: string[][];
    title: string;
    viewMode: 'documents';
  };
}

export type SavedObject = DashboardSavedObject | IndexPatternSavedObject | SearchSavedObject;
export type SavedObjectType = SavedObject['type'];
