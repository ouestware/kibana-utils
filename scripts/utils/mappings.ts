import { sortBy, mapValues } from 'lodash';
import { MappingProperty } from '@elastic/elasticsearch/lib/api/types';
import moment = require('moment');

// Number utils
export function castToNumber(s: string): number {
  return +s.trim();
}
export function isNumber(s: string): boolean {
  return !isNaN(castToNumber(s));
}

// Multi valued fields utils
const SEPARATORS = [';', ',', '|'] as const;
type Separator = typeof SEPARATORS[number];
export function getSeparator(values: string[]): string | null {
  const separatorsFrequencies = SEPARATORS.reduce(
    (iter, sep) => ({
      ...iter,
      [sep]: 0,
    }),
    {}
  ) as Record<Separator, number>;

  values.forEach((value) =>
    SEPARATORS.forEach((sep) => {
      const split = value.split(sep);
      if (split.length > 1 && split.every((s) => !!s && !s.match(/(^ | $)/)))
        separatorsFrequencies[sep]++;
    })
  );

  const bestSeparator = sortBy(
    SEPARATORS.filter((sep) => !!separatorsFrequencies[sep]),
    (sep) => -separatorsFrequencies[sep]
  )[0];
  return bestSeparator || null;
}

// Field type utils
type FieldType =
  | { type: 'number' }
  | { type: 'keyword'; separator?: string }
  | { type: 'date' }
  | { type: 'text' };
export function getFieldTypes(
  rows: Record<string, string>[],
  header: string[]
): Record<string, FieldType> {
  const uniqValues: Record<string, Set<string>> = {};
  const emptyValues: Record<string, number> = {};

  header.forEach((field) => {
    uniqValues[field] = new Set();
    emptyValues[field] = 0;
  });

  rows.forEach((row) => {
    header.forEach((field) => {
      if (field in row) {
        uniqValues[field].add(row[field]);
      } else {
        emptyValues[field]++;
      }
    });
  });

  const mappings: Record<string, FieldType> = {};
  header.forEach((field) => {
    const values = Array.from(uniqValues[field]);
    const differentValuesCount = values.length;
    const emptyValuesCount = emptyValues[field];
    const allValuesCount = rows.length - emptyValuesCount;

    if (values.filter((v) => isNumber(v)).length >= differentValuesCount * 0.7) {
      mappings[field] = {
        type: 'number',
      };
      return;
    }

    if (values.filter((v) => moment(v).isValid()).length >= differentValuesCount * 0.9) {
      mappings[field] = {
        type: 'date',
      };
      return;
    }

    const sep = getSeparator(values);
    if (sep) {
      mappings[field] = {
        type: 'keyword',
        separator: sep,
      };
    } else if (differentValuesCount <= allValuesCount * 0.8) {
      mappings[field] = {
        type: 'keyword',
      };
    } else {
      mappings[field] = {
        type: 'text',
      };
    }
  });

  return mappings;
}

// Mapping utils:
export function fieldTypesToMappings(
  fieldTypes: Record<string, FieldType>
): Record<string, MappingProperty> {
  return mapValues(fieldTypes, (fieldType) => {
    switch (fieldType.type) {
      case 'number':
        return { type: 'float' };
      case 'keyword':
        return { type: 'keyword' };
      case 'date':
        return { type: 'date' };
      case 'text':
      default:
        return { type: 'text' };
    }
  }) as Record<string, MappingProperty>;
}

export function formatDocument(
  row: Record<string, string>,
  fieldTypes: Record<string, FieldType>
): Record<string, any> {
  return mapValues(row, (value, field) => {
    if (!value) return undefined;

    const fieldType = fieldTypes[field] || { type: null };
    if (fieldType.type === 'keyword' && fieldType.separator)
      return value.split(fieldType.separator);
    if (fieldType.type === 'number') return castToNumber(value);
    if (fieldType.type === 'date') return moment(value).toISOString();

    return value;
  });
}
