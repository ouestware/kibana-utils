import path from 'path';
import dotenv from 'dotenv';
import { promises } from 'fs';
import { parse } from 'papaparse';
import { program } from 'commander';
import { Client } from '@elastic/elasticsearch';
import moment = require('moment');

import { fieldTypesToMappings, formatDocument, getFieldTypes } from '../utils/mappings';

// Command line arguments:
program.name('index-csv');
program.description('A script to help painlessly CSV files into ElasticSearch');
program.requiredOption('-d, --data <path>', 'specify the CSV data file path');
program.requiredOption('-x, --index <string>', 'specify the index name in ElasticSearch');
program.option('-e, --elastic <url>', 'specify the ElasticSearch address', 'http://localhost:9200');
program.option(
  '-v, --env <url>',
  'specify the env file with ',
  path.resolve(__dirname, '../../docker/.env')
);
program.parse();

const options = program.opts();
const CONFIG = {
  elastic: options.elastic as string,
  index: options.index as string,
  csv_path: options.data as string,
  env: options.env as string,
} as const;

async function run() {
  await dotenv.config({ path: CONFIG.env });

  const client = new Client({
    node: CONFIG.elastic,
  });
  console.log(`Read data CSV file at ${CONFIG.csv_path}`);
  const rawCSV = await promises.readFile(CONFIG.csv_path, 'utf8');
  const {
    data,
    meta: { fields },
  } = parse(rawCSV, { header: true });
  const rows = data as Record<string, any>[];
  const header = fields as string[];

  moment.suppressDeprecationWarnings = true;
  const fieldTypes = getFieldTypes(rows, header);
  console.log('Inferred mappings:');
  console.log(fieldTypes);

  try {
    await client.indices.delete({ index: CONFIG.index });
    console.log(`Existing index ${CONFIG.index} deleted`);
  } catch (e) {
    console.log(`No existing index ${CONFIG.index} found`);
  }

  console.log(`Create new index ${CONFIG.index}`);
  await client.indices.create({
    index: CONFIG.index,
    mappings: { properties: fieldTypesToMappings(fieldTypes) },
  });

  console.log(`Indexing ${rows.length} documents`);
  let indexedDocsCount = 0;
  for (const row of rows) {
    await client.index({
      index: CONFIG.index,
      document: formatDocument(row, fieldTypes),
    });
    indexedDocsCount++;

    if (!(indexedDocsCount % 100))
      console.log(
        `  -> ${indexedDocsCount} docs indexed (${((indexedDocsCount / rows.length) * 100).toFixed(
          2
        )}%)`
      );
  }
}

run().catch(console.log);
