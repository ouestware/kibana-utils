import path from 'path';
import dotenv from 'dotenv';
import { program } from 'commander';
import { Client } from '@elastic/elasticsearch';

const ROLES = ['reader'];

// Command line arguments:
program.name('add-user');
program.description('A script to create a user with read access to a given space');
program.requiredOption('-u, --username <string>', 'specify the user name');
program.requiredOption('-p, --password <string>', 'specify the user password');
program.requiredOption('-s, --space <string>', 'specify the space to add the user in');
program.option('-e, --elastic <url>', 'specify the ElasticSearch address', 'http://localhost:9200');
program.option(
  '-v, --env <url>',
  'specify the env file with ',
  path.resolve(__dirname, '../../docker/.env')
);
program.parse();

const options = program.opts();
const CONFIG = {
  username: options.username as string,
  password: options.password as string,
  elastic: options.elastic as string,
  kibana: options.kibana as string,
  space: options.space as string,
  env: options.env as string,
  role: 'reader',
} as const;

if (!ROLES.includes(CONFIG.role)) throw new Error(`Role ${CONFIG.role} is not recognized`);

async function run() {
  await dotenv.config({ path: CONFIG.env });
  const client = new Client({
    node: CONFIG.elastic,
  });

  await client.security.putUser({
    username: CONFIG.username,
    password: CONFIG.password,
    roles: [`${CONFIG.space}-${CONFIG.role}`],
  });

  console.log(`The user ${CONFIG.username} has properly been created`);
}

run().catch(console.log);
