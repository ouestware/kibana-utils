import path from 'path';
import dotenv from 'dotenv';
import { program } from 'commander';
import axios from 'axios';

import { slugify } from '../utils/strings';

// Command line arguments:
program.name('add-space');
program.description('A script to create a new space and the proper role, to visualize an index');
program.requiredOption('-n, --name <string>', 'specify the new space name');
program.requiredOption('-x, --index <string>', 'specify the ES index id');
program.option(
  '-s, --slug <string>',
  'specify the new space slug (defaults to the name, but slugified)'
);
program.option('-k, --kibana <url>', 'specify the Kibana address', 'http://localhost:5601');
program.option(
  '-v, --env <url>',
  'specify the env file with ',
  path.resolve(__dirname, '../../docker/.env')
);
program.parse();

const options = program.opts();
const CONFIG = {
  env: options.env as string,
  name: options.name as string,
  index: options.index as string,
  kibana: options.kibana as string,
  slug: (options.slug as string) || slugify(options.name as string),
} as const;

async function run() {
  await dotenv.config({ path: CONFIG.env });

  console.log(`Create new space ${CONFIG.name} (with id ${CONFIG.slug})`);
  await axios.post(
    `${CONFIG.kibana}/api/spaces/space`,
    { id: CONFIG.slug, name: CONFIG.name },
    {
      headers: {
        'kbn-xsrf': 'true',
        'Content-Type': 'application/json',
      },
    }
  );

  console.log(`Create new role ${CONFIG.slug}/read`);
  await axios.put(
    `${CONFIG.kibana}/api/security/role/${CONFIG.slug}-reader`,
    {
      elasticsearch: {
        cluster: [],
        indices: [
          {
            names: [CONFIG.index],
            privileges: ['read'],
          },
        ],
      },
      kibana: [
        {
          base: ['read'],
          feature: {},
          spaces: [CONFIG.slug],
        },
      ],
    },
    {
      headers: {
        'kbn-xsrf': 'true',
        'Content-Type': 'application/json',
      },
    }
  );

  console.log(`The role ${CONFIG.slug}-reader has properly been created`);
}

run().catch(console.log);
