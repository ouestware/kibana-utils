import path from 'path';
import axios from 'axios';
import dotenv from 'dotenv';
import ndjson from 'ndjson';
import { last } from 'lodash';
import FormData from 'form-data';
import fs, { promises } from 'fs';
import { file } from 'tmp-promise';
import { program } from 'commander';

import { SavedObject, SavedObjectType } from '../utils/kibana';

// Command line arguments:
program.name('import-dashboard');
program.description('A script to import a dumped dashboard to Kibana');
program.requiredOption(
  '-i, --index <string>',
  'specify the ES index to visualize in the dashboard'
);
program.requiredOption('-d, --dashboard <path>', 'specify the dashboard dump .ndjson file path');
program.option('-k, --kibana <url>', 'specify the Kibana address', 'http://localhost:5601');
program.option(
  '-v, --env <url>',
  'specify the env file with ',
  path.resolve(__dirname, '../../docker/.env')
);
program.parse();

const options = program.opts();
const CONFIG = {
  env: options.env as string,
  index: options.index as string,
  kibana: options.kibana as string,
  dashboard: options.dashboard as string,
} as const;

async function run() {
  await dotenv.config({ path: CONFIG.env });

  console.log(`Parse exported dashboard file at ${CONFIG.dashboard}`);
  const ndjsonContent = await new Promise<unknown[]>((resolve, reject) => {
    const dumpedObjects: unknown[] = [];
    fs.createReadStream(CONFIG.dashboard)
      .pipe(ndjson.parse())
      .on('data', (obj) => dumpedObjects.push(obj))
      .on('end', () => resolve(dumpedObjects))
      .on('error', (e) => reject(e));
  });
  const objects = ndjsonContent.slice(0, -1) as SavedObject[];
  const report = last(ndjsonContent);

  console.log(`Replace index name in exported index-patterns`);
  objects
    .filter(({ type }) => type === 'index-pattern')
    .forEach((indexPattern) => {
      indexPattern.attributes.title = CONFIG.index;
    });

  console.log(`Reserialize exported dashboard`);
  const ndjsonDashboard = [...objects, report].map((o) => JSON.stringify(o)).join('\n');

  console.log('Write a temporary file');
  const { path, cleanup } = await file({ postfix: '.ndjson' });
  await promises.writeFile(path, ndjsonDashboard);

  console.log('Import the dashboard');
  const formData = new FormData();
  formData.append('file', fs.createReadStream(path));
  const resp = await axios.postForm(
    `${CONFIG.kibana}/api/saved_objects/_import?createNewCopies=true`,
    formData,
    {
      headers: {
        'kbn-xsrf': 'true',
        'Content-Type': 'application/json',
      },
    }
  );
  console.log('  -> Response:');
  console.log(JSON.stringify(resp.data, null, '  '));

  console.log('Cleanup');
  await cleanup();

  const successResults = resp.data.successResults as {
    type: SavedObjectType;
    id: string;
    destinationId: string;
  }[];
  const newDashboardID = successResults.find(({ type }) => type === 'dashboard')!.destinationId;
  console.log(
    `The new dashboard should be available at address ${CONFIG.kibana}/app/dashboards#/view/${newDashboardID}`
  );
}

run().catch(console.log);
