import path from 'path';
import dotenv from 'dotenv';
import { program } from 'commander';
import axios from 'axios';

import { SavedObjectType } from '../utils/kibana';

// Command line arguments:
program.name('copy-dashboard');
program.description('A script to copy an existing dashboard to an existing space');
program.requiredOption('-s, --space <string>', 'specify the space slug');
program.requiredOption(
  '-i, --index <string>',
  'specify the ES index to visualize in the dashboard'
);
program.requiredOption('-d, --dashboard <string>', 'specify the dashboard ID to copy');
program.option('-k, --kibana <url>', 'specify the Kibana address', 'http://localhost:5601');
program.option(
  '-v, --env <url>',
  'specify the env file with ',
  path.resolve(__dirname, '../../docker/.env')
);
program.parse();

const options = program.opts();
const CONFIG = {
  env: options.env as string,
  space: options.space as string,
  index: options.index as string,
  kibana: options.kibana as string,
  dashboard: options.dashboard as string,
} as const;

async function run() {
  await dotenv.config({ path: CONFIG.env });

  console.log(`Copy existing dashboard ${CONFIG.dashboard} to new space`);
  const copyResp = await axios.post(
    `${CONFIG.kibana}/api/spaces/_copy_saved_objects`,
    {
      objects: [
        {
          id: CONFIG.dashboard,
          type: 'dashboard',
        },
      ],
      spaces: [CONFIG.space],
      includeReferences: true,
      createNewCopies: true,
    },
    {
      headers: {
        'kbn-xsrf': 'true',
        'Content-Type': 'application/json',
      },
    }
  );
  console.log('  -> Result:');
  console.log(JSON.stringify(copyResp.data, null, '  '));
  if (!copyResp.data[CONFIG.space].success)
    throw new Error('An error occurred while copying the dashboard.');

  const successResults = copyResp.data[CONFIG.space].successResults as {
    type: SavedObjectType;
    id: string;
    destinationId: string;
  }[];
  const indexPatternIDs = successResults
    .filter(({ type }) => type === 'index-pattern')
    .map(({ destinationId }) => destinationId);

  if (indexPatternIDs.length) {
    console.log(
      `Update newly copied index-pattern${indexPatternIDs.length > 1 ? 's' : ''} so that ${
        indexPatternIDs.length > 1 ? 'they are' : 'it is'
      } linked to index ${CONFIG.index}`
    );
  }
  for (const indexPatternID of indexPatternIDs) {
    const indexPatternUpdateResp = await axios.put(
      `${CONFIG.kibana}/s/${CONFIG.space}/api/saved_objects/index-pattern/${indexPatternID}`,
      {
        attributes: {
          title: CONFIG.index,
        },
      },
      {
        headers: {
          'kbn-xsrf': 'true',
          'Content-Type': 'application/json',
        },
      }
    );
    console.log(`  -> Resp for ${indexPatternID}`);
    console.log(indexPatternUpdateResp.data);
  }

  const newDashboardID = successResults.find(({ type }) => type === 'dashboard')!.destinationId;
  console.log(
    `The new dashboard should be available at address ${CONFIG.kibana}/s/${CONFIG.space}/app/dashboards#/view/${newDashboardID}`
  );
}

run().catch(console.log);
